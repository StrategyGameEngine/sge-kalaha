package dev.entze.sge.game.mancala;

import dev.entze.sge.game.ActionRecord;
import dev.entze.sge.game.Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Kalaha implements Game<Integer, Integer[]> {

  public static final String boardRegex = "^.*\\((?<houses>\\d+),\\s*(?<seeds>\\d+)\\)$";
  public static final Pattern boardPattern = Pattern.compile(boardRegex);

  public static final int SOUTH = 0;
  public static final int NORTH = 1;
  public static final int DEFAULT_HOUSES = 6;
  public static final int DEFAULT_SEEDS = 6;
  private final boolean canonical;
  private final List<ActionRecord<Integer>> actionRecords;
  private final int[] board;
  private int currentPlayer;

  public Kalaha() {
    this(DEFAULT_HOUSES, DEFAULT_SEEDS);
  }

  public Kalaha(int houses, int seeds) {
    currentPlayer = SOUTH;
    canonical = true;
    actionRecords = new ArrayList<>();
    board = new int[(houses + 1) * 2];
    Arrays.fill(board, seeds);
    board[board.length / 2 - 1] = 0;
    board[board.length - 1] = 0;
  }

  public Kalaha(int currentPlayer, boolean canonical, List<ActionRecord<Integer>> actionRecords,
      int[] board) {
    this.currentPlayer = currentPlayer;
    this.canonical = canonical;
    this.actionRecords = new ArrayList<>(actionRecords);
    this.board = board.clone();
  }

  public Kalaha(Kalaha kalaha) {
    this(kalaha.currentPlayer, kalaha.canonical, kalaha.actionRecords, kalaha.board);
  }

  public Kalaha(String board, int numberOfPlayers) {
    this(Kalaha.extractHouses(board), Kalaha.extractSeeds(board));
    if (numberOfPlayers != 2) {
      throw new IllegalArgumentException("2 player game");
    }
  }

  public static int extractHouses(String board) {
    if (board != null) {
      Matcher matcher = boardPattern.matcher(board);
      if (matcher.matches()) {
        String housesString = matcher.group("houses");
        return Integer.parseInt(housesString);
      }
    }
    return DEFAULT_HOUSES;
  }

  public static int extractSeeds(String board) {
    if (board != null) {
      Matcher matcher = boardPattern.matcher(board);
      if (matcher.matches()) {
        String seedsString = matcher.group("seeds");
        return Integer.parseInt(seedsString);
      }
    }
    return DEFAULT_SEEDS;
  }

  @Override
  public boolean isGameOver() {
    boolean gameOver = true;
    for (int i = rowStart(); i <= rowEnd() && gameOver; i++) {
      gameOver = board[i] <= 0;
    }
    return gameOver;
  }

  @Override
  public int getMinimumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getMaximumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getCurrentPlayer() {
    return currentPlayer;
  }

  @Override
  public double getUtilityValue(int player) {
    return board[rowEnd(player) + 1];
  }

  @Override
  public double getHeuristicValue(int player) {
    int heuristic = 0;
    for (int i = rowStart(player); i <= store(player); i++) {
      heuristic += board[i];
    }
    return heuristic;
  }

  @Override
  public Set<Integer> getPossibleActions() {
    Set<Integer> possibleActions = new TreeSet<>();
    for (int i = rowStart(); i <= rowEnd(); i++) {
      if (board[i] > 0) {
        possibleActions.add(i);
      }
    }
    return possibleActions;
  }

  @Override
  public Integer[] getBoard() {
    Integer[] board = new Integer[this.board.length];

    for (int i = 0; i < this.board.length; i++) {
      board[i] = this.board[i];
    }

    return board;
  }

  @Override
  public boolean isValidAction(Integer action) {
    return rowStart() <= action && action <= rowEnd() && board[action] > 0;
  }

  @Override
  public Game<Integer, Integer[]> doAction(Integer action) {

    if (!(rowStart() <= action && action <= rowEnd())) {
      throw new IllegalArgumentException("Cannot take seeds from house " + action);
    }

    if (board[action] <= 0) {
      throw new IllegalArgumentException("House " + action + " is empty");
    }

    Kalaha next = new Kalaha(this);

    int house = action;
    int stones = next.board[house];
    next.board[house] = 0;

    while (stones > 0) {
      house = (house + 1) % board.length;
      if (store(1 - currentPlayer) != house) {
        stones--;
        next.board[house]++;
        if (stones <= 0) {
          if (rowStart() <= house && house <= rowEnd() && next.board[house] == 1) {
            next.capture(house);
          } else if (store() == house) {
            next.currentPlayer = 1 - currentPlayer;
          }
        }
      }
    }

    next.actionRecords.add(new ActionRecord<>(currentPlayer, action));
    next.currentPlayer = 1 - next.currentPlayer;

    if (next.isGameOver()) {
      for (int i = rowStart(1 - next.currentPlayer); i <= rowEnd(1 - next.currentPlayer); i++) {
        next.moveToStore(i, 1 - next.currentPlayer);
      }
    }

    return next;
  }

  @Override
  public Integer determineNextAction() {
    return null;
  }

  @Override
  public List<ActionRecord<Integer>> getActionRecords() {
    return Collections.unmodifiableList(actionRecords);
  }

  @Override
  public boolean isCanonical() {
    return canonical;
  }

  @Override
  public Game<Integer, Integer[]> getGame(int player) {
    return new Kalaha(currentPlayer, false, actionRecords, board);
  }

  @Override
  public String toTextRepresentation() {
    String[] boardAsStrings = new String[board.length];

    int maxWidth = (-1);

    for (int i = 0; i < board.length; i++) {
      boardAsStrings[i] = "" + board[i];
      maxWidth = Math.max(maxWidth, boardAsStrings[i].length());
    }

    maxWidth += 2;

    for (int i = 0; i < boardAsStrings.length; i++) {
      for (int j = 0; boardAsStrings[i].length() < maxWidth; j++) {
        if (j % 2 == 0) {
          boardAsStrings[i] = " ".concat(boardAsStrings[i]);
        } else {
          boardAsStrings[i] = boardAsStrings[i].concat(" ");
        }
      }
    }

    String ceiling = "+---";
    String whitespace = "";

    for (int i = 0; i < maxWidth; i++) {
      whitespace = whitespace.concat(" ");
    }

    while (ceiling.length() - 1 < maxWidth) {
      ceiling = ceiling.concat("-");
    }

    StringBuilder stringBuilder = new StringBuilder().append('\n');

    int playerIndicator = ((maxWidth * (board.length / 2 + 1) + board.length / 2 + 2) - 9) / 2 - 1;

    if (currentPlayer == Kalaha.NORTH) {
      for (int i = 0; i <= playerIndicator; i++) {
        stringBuilder.append(' ');
      }
      stringBuilder.append("<-- NORTH\n");
    }

    for (int i = 0; i < board.length / 2 + 1; i++) {
      stringBuilder.append(ceiling);
    }

    stringBuilder.append("+\n|").append(whitespace);

    stringBuilder.append('|');
    for (int i = board.length - 2; i >= board.length / 2; i--) {
      stringBuilder.append(boardAsStrings[i]).append('|');
    }
    stringBuilder.append(whitespace).append("|\n|")
        .append(boardAsStrings[board.length - 1]);
    for (int i = 0; i < board.length / 2 - 1; i++) {
      stringBuilder.append(ceiling);
    }
    stringBuilder.append('+').append(boardAsStrings[board.length / 2 - 1]).append("|\n|")
        .append(whitespace).append('|');

    for (int i = 0; i < board.length / 2 - 1; i++) {
      stringBuilder.append(boardAsStrings[i]).append('|');
    }
    stringBuilder.append(whitespace).append("|\n");

    for (int i = 0; i < board.length / 2 + 1; i++) {
      stringBuilder.append(ceiling);
    }
    stringBuilder.append("+\n");

    if (currentPlayer == Kalaha.SOUTH) {
      for (int i = 0; i <= playerIndicator; i++) {
        stringBuilder.append(' ');
      }
      stringBuilder.append("SOUTH -->\n");
    }

    return stringBuilder.toString();
  }

  @Override
  public String toString() {
    return Arrays.toString(board);
  }

  private void moveToStore(int house) {
    moveToStore(house, houseBelongsTo(house));
  }

  private void moveToStore(int house, int player) {
    board[store(player)] += board[house];
    board[house] = 0;
  }

  private void capture(int house) {
    capture(house, houseBelongsTo(house));
  }

  private void capture(int house, int store) {
    moveToStore(house, store);
    moveToStore(oppositeHouse(house), store);
  }

  private int oppositeHouse(int house) {
    return (board.length - 2) - house;
  }

  private int rowStart() {
    return rowStart(currentPlayer);
  }

  private int rowStart(int player) {
    return (board.length / 2) * player;
  }

  private int rowEnd() {
    return rowEnd(currentPlayer);
  }

  private int rowEnd(int player) {
    return board.length / (2 - player) - 2;
  }

  private int store() {
    return store(currentPlayer);
  }

  private int store(int player) {
    return board.length / (2 - player) - 1;
  }

  private int houseBelongsTo(int house) {
    return house / (board.length / 2);
  }

}
