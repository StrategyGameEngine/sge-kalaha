package dev.entze.sge.game.mancala;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import dev.entze.sge.game.Game;
import java.util.Collections;
import org.junit.Test;

public class KalahaTest {

  private Game<Integer, Integer[]> kalaha;

  @Test
  public void test_visualisation_1() {
    kalaha = new Kalaha(6, 6);

    assertEquals("\n" +
            "+---+---+---+---+---+---+---+---+\n" +
            "|   | 6 | 6 | 6 | 6 | 6 | 6 |   |\n" +
            "| 0 +---+---+---+---+---+---+ 0 |\n" +
            "|   | 6 | 6 | 6 | 6 | 6 | 6 |   |\n" +
            "+---+---+---+---+---+---+---+---+\n" +
            "            SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_2() {
    kalaha = new Kalaha(6, 6);
    kalaha = kalaha.doAction(1);
    assertEquals("\n" +
            "            <-- NORTH\n" +
            "+---+---+---+---+---+---+---+---+\n" +
            "|   | 6 | 6 | 6 | 6 | 6 | 7 |   |\n" +
            "| 0 +---+---+---+---+---+---+ 1 |\n" +
            "|   | 6 | 0 | 7 | 7 | 7 | 7 |   |\n" +
            "+---+---+---+---+---+---+---+---+\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_3() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
    assertEquals("\n" +
            "+----+----+----+----+----+----+----+----+\n" +
            "|    | 12 | 11 | 10 |  9 |  8 |  7 |    |\n" +
            "| 13 +----+----+----+----+----+----+  6 |\n" +
            "|    |  0 |  1 |  2 |  3 |  4 |  5 |    |\n" +
            "+----+----+----+----+----+----+----+----+\n" +
            "                SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_4() {
    kalaha = new Kalaha(Kalaha.NORTH, true, Collections.emptyList(),
        new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
    assertEquals("\n" +
            "                <-- NORTH\n" +
            "+----+----+----+----+----+----+----+----+\n" +
            "|    | 12 | 11 | 10 |  9 |  8 |  7 |    |\n" +
            "| 13 +----+----+----+----+----+----+  6 |\n" +
            "|    |  0 |  1 |  2 |  3 |  4 |  5 |    |\n" +
            "+----+----+----+----+----+----+----+----+\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_5() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {0, 10, 2, 300, 4, 50, 600, 7, 80, 9, 100, 1, 120, 3});
    assertEquals("\n" +
            "+-----+-----+-----+-----+-----+-----+-----+-----+\n" +
            "|     | 120 |  1  | 100 |  9  |  80 |  7  |     |\n" +
            "|  3  +-----+-----+-----+-----+-----+-----+ 600 |\n" +
            "|     |  0  |  10 |  2  | 300 |  4  |  50 |     |\n" +
            "+-----+-----+-----+-----+-----+-----+-----+-----+\n" +
            "                    SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_6() {
    kalaha = new Kalaha(Kalaha.NORTH, true, Collections.emptyList(),
        new int[] {0, 10, 2, 300, 4, 50, 600, 7, 80, 9, 100, 1, 120, 3});
    assertEquals("\n" +
            "                    <-- NORTH\n" +
            "+-----+-----+-----+-----+-----+-----+-----+-----+\n" +
            "|     | 120 |  1  | 100 |  9  |  80 |  7  |     |\n" +
            "|  3  +-----+-----+-----+-----+-----+-----+ 600 |\n" +
            "|     |  0  |  10 |  2  | 300 |  4  |  50 |     |\n" +
            "+-----+-----+-----+-----+-----+-----+-----+-----+\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_7() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {1, 2, 3, 4, 5, 6});
    assertEquals("\n" +
            "+---+---+---+---+\n" +
            "|   | 5 | 4 |   |\n" +
            "| 6 +---+---+ 3 |\n" +
            "|   | 1 | 2 |   |\n" +
            "+---+---+---+---+\n" +
            "    SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_8() {
    kalaha = new Kalaha(Kalaha.NORTH, true, Collections.emptyList(),
        new int[] {1, 2, 3, 4, 5, 6});
    assertEquals("\n" +
            "    <-- NORTH\n" +
            "+---+---+---+---+\n" +
            "|   | 5 | 4 |   |\n" +
            "| 6 +---+---+ 3 |\n" +
            "|   | 1 | 2 |   |\n" +
            "+---+---+---+---+\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_9() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {1000, 2000, 3000, 400, 50, 6});
    assertEquals("\n" +
            "+------+------+------+------+\n" +
            "|      |  50  |  400 |      |\n" +
            "|   6  +------+------+ 3000 |\n" +
            "|      | 1000 | 2000 |      |\n" +
            "+------+------+------+------+\n" +
            "          SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_10() {
    kalaha = new Kalaha(Kalaha.NORTH, true, Collections.emptyList(),
        new int[] {1000, 2000, 3000, 400, 50, 6});
    assertEquals("\n" +
            "          <-- NORTH\n" +
            "+------+------+------+------+\n" +
            "|      |  50  |  400 |      |\n" +
            "|   6  +------+------+ 3000 |\n" +
            "|      | 1000 | 2000 |      |\n" +
            "+------+------+------+------+\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_visualisation_11() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {10000, 2000, 3000, 400, 50, 6});
    assertEquals("\n" +
            "+-------+-------+-------+-------+\n" +
            "|       |   50  |  400  |       |\n" +
            "|   6   +-------+-------+  3000 |\n" +
            "|       | 10000 |  2000 |       |\n" +
            "+-------+-------+-------+-------+\n" +
            "            SOUTH -->\n",
        kalaha.toTextRepresentation());

  }

  @Test
  public void test_doAction_1() {
    kalaha = new Kalaha(6, 6);
    kalaha = kalaha.doAction(0);
    assertEquals(Kalaha.SOUTH, kalaha.getCurrentPlayer());
    assertFalse(kalaha.isGameOver());
    assertEquals(0, (int) kalaha.getPreviousAction());
    assertEquals(1L, (long) kalaha.getUtilityValue());
  }

  @Test
  public void test_doAction_2() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {0, 1, 0, 10, 10, 0});
    assertEquals(0L, (long) kalaha.getUtilityValue());
    kalaha = kalaha.doAction(1);
    assertEquals(-19L, (long) kalaha.getUtilityValue());
    assertTrue(kalaha.isGameOver());
  }

  @Test
  public void test_doAction_3() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {1, 0, 0, 10, 10, 0});
    assertEquals(0L, (long) kalaha.getUtilityValue());
    kalaha = kalaha.doAction(0);
    assertFalse(kalaha.isGameOver());
    assertArrayEquals(new Integer[] {0, 0, 11, 0, 10, 0}, kalaha.getBoard());
    assertEquals(11L, (long) kalaha.getUtilityValue(1D, -1D));
  }

  @Test
  public void test_doAction_4() {
    kalaha = new Kalaha(Kalaha.SOUTH, true, Collections.emptyList(),
        new int[] {1, 0, 10, 0, 0, 10, 0, 0});
    assertEquals(0L, (long) kalaha.getUtilityValue());
    kalaha = kalaha.doAction(0);
    assertTrue(kalaha.isGameOver());
    assertArrayEquals(new Integer[] {0, 0, 0, 21, 0, 0, 0, 0}, kalaha.getBoard());
    assertEquals(21L, (long) kalaha.getUtilityValue(1D, -1D));
  }

  @Test

  public void test_doAction_5() {
    kalaha = new Kalaha(1, 2);
    assertEquals(0L, (long) kalaha.getUtilityValue());
    kalaha = kalaha.doAction(0);
    assertFalse(kalaha.isGameOver());
    assertArrayEquals(new Integer[] {0, 1, 3, 0}, kalaha.getBoard());
    assertEquals(1L, (long) kalaha.getUtilityValue(1D, -1D));
    kalaha = kalaha.doAction(2);
    assertTrue(kalaha.isGameOver());
    assertArrayEquals(new Integer[] {0, 1, 0, 3}, kalaha.getBoard());
    assertEquals(-2L, (long) kalaha.getUtilityValue(1D, -1D));

  }

  @Test
  public void test_doAction_6() {
    kalaha = new Kalaha(Kalaha.NORTH, true, Collections.emptyList(), new int[] {1, 1, 0, 1, 0, 0});
    kalaha = kalaha.doAction(3);
    assertArrayEquals(new Integer[] {0, 1, 0, 0, 0, 2}, kalaha.getBoard());
  }

}